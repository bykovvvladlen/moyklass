const app = require('../src/app.js');
const request = require('supertest');
const server = request(app.callback());
const moment = require('moment');

describe('create lessons', () => {
    it('should create lessons with specified parameters', async () => {
        const request = await server
            .post('/lessons')
            .send({
                title: 'Test Lesson',
                teachersIds: [1, 3],
                firstDate: '2005-03-08',
                days: [1, 4],
                lessonsCount: 14,
            })
            .expect(200);

        expect(Array.isArray(request.body.lessonsIds)).toBeTruthy();
        expect(request.body.lessonsIds.length).toEqual(14);
    });

    it('should create 300 lessons', async () => {
        const request = await server
            .post('/lessons')
            .send({
                title: 'Test Lesson',
                teachersIds: [1],
                firstDate: '2007-03-08',
                lastDate: '2009-03-08',
                days: [0, 1, 2, 3, 4, 5, 6],
            })
            .expect(200);

        expect(Array.isArray(request.body.lessonsIds)).toBeTruthy();
        expect(request.body.lessonsIds?.length).toEqual(300);
    });
});

describe('get lessons', () => {
    it('should return lessons', async () => {
        const request = await server
            .get('/')
            .expect(200);

        expect(Array.isArray(request.body)).toBeTruthy();
        expect(request.body.length).toBeGreaterThan(0);
    });

    it('should return only one lesson', async () => {
        const request = await server
            .get('/?lessonsPerPage=1')
            .expect(200);

        expect(Array.isArray(request.body)).toBeTruthy();
        expect(request.body.length).toEqual(1);
    });

    it('should return lesson with zero status', async () => {
        const request = await server
            .get('/?status=0')
            .expect(200);

        expect(Array.isArray(request.body)).toBeTruthy();
        const { status } = request.body.shift();
        expect(status).toEqual(0);
    });

    it('should return lesson with correct students count', async () => {
        const request = await server
            .get('/?studentsCount=3')
            .expect(200);

        expect(Array.isArray(request.body)).toBeTruthy();
        expect(request.body.shift()?.students.length).toEqual(3);
    });

    it('should return lessons with correct date range', async () => {
        const firstDate = '2005-03-08';
        const lastDate = '2005-05-01';
        const request = await server
            .get('/')
            .query({ date: '2005-03-08,2005-05-01' })
            .expect(200);

        expect(Array.isArray(request.body)).toBeTruthy();
        const lessons = request.body;
        const firstDateMoment = moment(firstDate);
        const lastDateMoment = moment(lastDate);
        
        for (let { date } of lessons) {
            const lessonDateMoment = moment(date);
            expect(lessonDateMoment >= firstDateMoment && lessonDateMoment <= lastDateMoment).toBeTruthy();
        }
    });
});