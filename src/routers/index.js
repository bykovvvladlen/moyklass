const Router = require('@koa/router');
const router = new Router();
const knex = require('../knex');
const lessons = require('./lessons');

router.use(lessons.routes());
router.get('/', async ctx => {
    const dates = ctx
        .checkQuery('date')
        .optional()
        .notEmpty()
        .match(/^(,?\d{4}-\d{2}-\d{2}){1,2}$/)
        .value
        ?.split(',');

    const status = ctx
        .checkQuery('status')
        .optional()
        .notEmpty()
        .match(/1|0/)
        .isInt()
        .value;

    const teachersIds = ctx
        .checkQuery('teachersIds')
        .optional()
        .notEmpty()
        .match(/^(,?\d+){1,}$/)
        .value
        ?.split(',');

    const studentsCount = ctx
        .checkQuery('studentsCount')
        .optional()
        .notEmpty()
        .match(/^(,?\d+){1,2}$/)
        .value
        ?.split(',');

    const page = ctx
        .checkQuery('page')
        .optional()
        .notEmpty()
        .isInt()
        .default(1)
        .toInt()
        .value;

    const lessonsPerPage = ctx
        .checkQuery('lessonsPerPage')
        .optional()
        .notEmpty()
        .isInt()
        .default(5)
        .toInt()
        .value;

    const offset = (page - 1) * lessonsPerPage;

    if (ctx.errors) {
        ctx.body = ctx.errors;
        ctx.status = 400;
        return;
    }

    const lessonsQuery = knex('lessons');
    
    if (dates) {
        if (dates.length === 1) {
            lessonsQuery.where('date', dates[0]);
        }

        else {
            lessonsQuery.whereBetween('date', dates);
        }
    }

    if (status) {
        lessonsQuery.where('status', status);
    }

    lessonsQuery
        .select('lessons.*')
        .leftJoin('lesson_students', 'lessons.id', '=', 'lesson_students.lesson_id')
        .groupBy('lessons.id');
    
    if (teachersIds) {
        lessonsQuery.whereIn('lessons.id', knex.raw(`SELECT DISTINCT lesson_id FROM lesson_teachers WHERE teacher_id IN (${teachersIds.join()})`));
    }

    if (studentsCount) {
        if (studentsCount.length === 1) {
            lessonsQuery.having(knex.raw(`count(lesson_students) = ${studentsCount[0]}`));
        }

        else {
            lessonsQuery
                .having(knex.raw(`count(lesson_students) >= ${studentsCount[0]}`))
                .having(knex.raw(`count(lesson_students) <= ${studentsCount[1]}`));
        }
    }

    const lessons = await lessonsQuery.limit(lessonsPerPage).offset(offset);
    const lessonsIds = lessons.map(({ id }) => id);

    const allTeachers = await knex('lesson_teachers')
        .whereIn('lesson_id', lessonsIds)
        .join('teachers', 'teachers.id', '=', 'lesson_teachers.teacher_id');

    const allStudents = await knex('lesson_students')
        .whereIn('lesson_id', lessonsIds)
        .join('students', 'students.id', '=', 'lesson_students.student_id');

    const grouppedStudents = allStudents.reduce((acc, student) => {
        const { name, visit } = student;
        if (!acc[student.lesson_id]) acc[student.lesson_id] = [];
        acc[student.lesson_id].push({ name, visit });
        return acc;
    }, {});

    const grouppedTeachers = allTeachers.reduce((acc, teacher) => {
        const { name } = teacher;
        if (!acc[teacher.lesson_id]) acc[teacher.lesson_id] = [];
        acc[teacher.lesson_id].push({ name });
        return acc;
    }, {});

    ctx.body = lessons.map(lesson => {
        lesson.students = grouppedStudents[lesson.id] ?? [];
        lesson.teachers = grouppedTeachers[lesson.id] ?? [];
        lesson.visitCount = lesson.students.filter(({ visit }) => visit).length; 
        return lesson;
    });
});

module.exports = router;