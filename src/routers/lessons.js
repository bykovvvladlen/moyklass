const Router = require('@koa/router');
const router = new Router({ prefix: '/lessons' });
const knex = require('../knex');
const moment = require('moment');

router.post('/', async ctx => {
    const teachersIds = ctx
        .checkBody('teachersIds')
        .notEmpty()
        .value;

    if (!Array.isArray(teachersIds)) {
        if (!ctx.errors) ctx.errors = [];
        ctx.errors.push({ teachersIds: 'teachersIds must be an array.' });
    }

    const title = ctx
        .checkBody('title')
        .notEmpty()
        .value;

    const days = ctx
        .checkBody('days')
        .notEmpty()
        .value;

    if (!Array.isArray(days)) {
        if (!ctx.errors) ctx.errors = [];
        ctx.errors.push({ days: 'days must be an array.' });
    }

    const firstDate = ctx
        .checkBody('firstDate')
        .notEmpty()
        .isDate()
        .value;

    const lessonsCount = ctx
        .checkBody('lessonsCount')
        .optional()
        .notEmpty()
        .isInt()
        .gt(0)
        .value;

    const lastDate = ctx
        .checkBody('lastDate')
        .optional()
        .notEmpty()
        .isDate()
        .gt(firstDate)
        .value;

    if (Boolean(lastDate) === Boolean(lessonsCount)) {
        if (!ctx.errors) ctx.errors = [];
        ctx.errors.push({ 
            error: Boolean(lastDate) ? 
            'lastDate cannot be specified together with lessonsCount.' 
            : 'specify lastDate or lessonsCount.',
        });
    }

    if (ctx.errors) {
        ctx.body = ctx.errors;
        ctx.status = 400;
        return;
    }

    const firstDateMoment = moment(firstDate);
    const maxLastDateMoment = moment(firstDate).add(1, 'year');
    const specifiedLastDateMoment = lastDate ? moment(lastDate) : maxLastDateMoment;
    const maxDateMoment = moment.min(specifiedLastDateMoment, maxLastDateMoment);
    const lessons = [];

    for (let week = 0; week < Math.ceil(365/7); week++) {
        for (let day = 0; day < days.length; day++) {
            const date = moment(firstDateMoment).day(days[day] + 7 * week);
            if (date < firstDateMoment) continue;
            else if (date > maxDateMoment) break;

            lessons.push({
                title,
                status: 0,
                date: date.format('YYYY-MM-DD'),
            });
        }
    }

    const lessonsIds = await knex.insert(lessons.splice(0, lessonsCount ?? 300), ['id']).into('lessons');
    const lessonTeachers = lessonsIds.map(lesson => teachersIds.map(teacher_id => ({ lesson_id: lesson.id, teacher_id }))).flat();
    await knex.insert(lessonTeachers).into('lesson_teachers');
    ctx.body = { lessonsIds: lessonsIds.map(({ id }) => id) };
});

module.exports = router;