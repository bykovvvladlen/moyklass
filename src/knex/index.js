require('dotenv').config();
const knex = require('knex')({
    connection: process.env.PG_CONNECTION_STRING,
    client: 'pg',
});

module.exports = knex;