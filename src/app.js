const Koa = require('koa');
const router = require('./routers');
const app = new Koa();

require('koa-validate')(app);
app.use(require('koa-body')({
    multipart: true, 
    formidable: {
        keepExtensions: true,
    },
}));

app.use(router.routes()).use(router.allowedMethods());

if (!module.parent) {
    const { PORT } = process.env;
    app.listen(PORT);

    console.log(`App listen on port ${PORT}`);
}

module.exports = app;